<?php
/**
 * @file
 * Contains Login Settings for miniOrange SAML Login Module.
 */

 /**
 * Showing Settings form.
 */
 namespace Drupal\miniorange_saml\Form;

 use Drupal\Core\Form\FormStateInterface;
 use Drupal\Core\Form\FormBase;
 use Drupal\miniorange_saml\Utilities;
 use Drupal\miniorange_saml\MiniorangeSAMLConstants;

 class MiniorangeSignonSettings extends FormBase {

  public function getFormId() {
    return 'miniorange_saml_login_setting';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

      $base_url = \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getBaseUrl();
      $config    = \Drupal::config('miniorange_saml.settings');
      \Drupal::configFactory()->getEditable('miniorange_saml.settings')->set('miniorange_saml_last_tab_visited', 'SIGNIN SETTINGS')->save();

      $form['miniorange_saml_markup_library'] = array(
        '#attached' => array(
          'library' => array(
            'miniorange_saml/miniorange_saml.admin',
            'core/drupal.dialog.ajax',
          )
        ),
      );

      $disabled = TRUE;

      $form['markup_2'] = array(
        '#markup' =>'<div class="mo_saml_font_for_heading_none_float">SIGNIN SETTINGS <a href="' . $base_url . MiniorangeSAMLConstants::LICENSING_TAB_URL .'">[Standard, Premium and Enterprise]</a></div><hr>',

      );

      $form['miniorange_saml_load_user_by']=array(
        '#type'=>'radios',
        '#title' => 'Search User in Drupal using the following attribute',
        '#options' => array(
          'Username' => $this->t('Username'),
          'Email' => $this->t('Email')
        ),
        '#default_value'=> is_null( $config->get('miniorange_saml_load_user_by' ) ) ? 'Username' : $config->get('miniorange_saml_load_user_by'),
        '#disabled' => $disabled,
      );

    $form['miniorange_saml_default_relaystate'] = array(
      '#type' => 'textfield',
      '#title' => t('Default Redirect URL after login'),
      '#default_value' => $config->get('miniorange_saml_default_relaystate'),
      '#attributes' => array('placeholder' => 'Enter Default Redirect URL'),
      '#disabled' => $disabled,
    );

    $form['miniorange_saml_default_redirect_url_after_logout'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Default Redirect URL after logout'),
      '#default_value' => $config->get('miniorange_saml_default_redirect_url_after_logout'),
      '#attributes'    => array('placeholder' => 'Enter Default Redirect URL after logout'),
      '#disabled'      => $disabled,
    );

    $form['miniorange_saml_redirect_url_invalid_status'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Redirect URL for Failed Status'),
      '#default_value' => $config->get('miniorange_saml_redirect_url_invalid_status'),
      '#attributes'    => array('placeholder' => 'Enter URL To Redirect On Failed Status'),
      '#disabled'      => $disabled,
      '#description'   => t('<b>Note:</b> Enter the URL to redirect the user when status present in the SAML response in not success. Enter the path excluding the base path of the website.'),
    );
    /* Site Domain SSO New */
    $form['miniorange_saml_domain_based_sso'] = array(
      '#type' => 'checkbox',
      '#title' => t('Site Domain Based SSO (For multisite setup only)'),
      '#disabled' => $disabled,
      '#description' => $this->t('<b>Note: </b>Enabling this option will be redirected to the configured IDP with which the application name matches.'),
    );

    $form['miniorange_saml_domain_based_sso_fieldset'] = array(
      '#type' => 'fieldset',
    );

    $form['miniorange_saml_domain_based_sso_fieldset']['miniorange_saml_domain_based_sso_textarea'] =array(
      '#type' => 'textarea',
      '#placeholder' => t('Enter semicolon(;) separated Site base URL and Application name &#10;(https://site1.com=>app_name_1; https://site2.com=>app_name_2)'),
      '#disabled' => $disabled,
      '#description' => $this->t('<b>Note: </b>This configuration is for multisite setup only. Enter the site\'s base URL and Application name pair for each application that you want to redirect to.
                            E.g., https://site1.com=>app_name1;https://site2.com=>app_name_2</span>'),
    );

    $form['miniorange_saml_disable_autocreate_users'] = array(
			'#type' => 'checkbox',
			'#title' => t('Disable auto creation of users if user does not exist'),
			'#description' => t('<b>Note :</b> If you enable this feature new user wont be created, only existing users can login using SSO.'),
			'#disabled' => $disabled,
		);

		$form['miniorange_saml_force_auth'] = array(
			'#type' => 'checkbox',
			'#title' => t('Protect website against anonymous access'),
			'#default_value' => $config->get('miniorange_saml_force_auth'),
			'#description' => t('<b>Note: </b>Users will be redirected to your IdP for login in case user is not logged in and tries to access website.'),
			'#disabled' => $disabled,
		);
    $form['miniorange_saml_auto_redirect'] = array(
			'#type' => 'checkbox',
			'#title' => t('Auto redirect the user to IdP'),
			'#default_value' => $config->get('miniorange_saml_auto_redirect_to_idp'),
			'#description' => t('<b>Note: </b>Users will be redirected to your IdP for login when the login page is accessed.'),
			'#disabled' => $disabled,
		);

    $form['miniorange_saml_enable_backdoor'] = array(
			'#type' => 'checkbox',
			'#title' => t('Enable backdoor login'),
			'#default_value' => $config->get('miniorange_saml_enable_backdoor'),
			'#description' => t('<b>Note: </b>Checking this option creates a backdoor to login to your website using Drupal credentials
                  in case you get locked out of your IdP.'),
			'#disabled' => $disabled,
		);

    $form['miniorange_saml_default_domain_mapping'] = array(
      '#type' => 'checkbox',
      '#disabled' => 'disabled ',
      '#title' => t('Domain Mapping'),
    );

    $form['domain_mapping_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('Domain Mapping Table'),
    );

    $form['domain_mapping_fieldset']['domain_mapping_fieldset_table'] = array(
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('IDP Name'), 'width' => '40%'],
        ['data' => $this->t('Default Domains	'), 'width' => '40%'],
        ['data' => $this->t('Operation'), 'width' => '20%'],
      ],
    );
    $form['domain_mapping_fieldset']['domain_mapping_fieldset_table'][0]['idp_name'] = array(
      '#type' => 'textfield',
      '#disabled' => true,
      '#placeholder' => 'Enter IDP Name',
    );
    $form['domain_mapping_fieldset']['domain_mapping_fieldset_table'][0]['default_domains'] = array(
      '#type' => 'textfield',
      '#size' => 15,
      '#disabled' => true,
      '#attributes' => array(
        'placeholder' => $this->t('semi-colon(;) separated')
      )
    );
    $form['domain_mapping_fieldset']['domain_mapping_fieldset_table'][0]['delete_button'] = array(
      '#markup' => '<span class="button">Remove</span>',
    );

    $form['domain_mapping_fieldset']['domain_mapping_add_button'] = [
    '#prefix' => '<div class="container-inline">',
    '#type' => 'submit',
    '#value' => 'Add',
    '#disabled' => TRUE,
    ];

    $form['domain_mapping_fieldset']['domain_mapping_total_items'] = [
    '#type' => 'number',
    '#default_value' => 1,
    '#min' => 1,
    '#max' => 50,
    '#suffix' => '&nbsp;&nbsp;more rows</div>',
    ];


    $form['miniorange_saml_domain_restriction_checkbox'] = array(
      '#type' => 'checkbox',
      '#title' => t('Domain Restriction'),
      '#disabled' => TRUE,
    );
    $form['miniorange_saml_set_of_radiobuttons'] = array(
      '#type' => 'fieldset',
    );
    $form['miniorange_saml_set_of_radiobuttons']['miniorange_saml_allow_or_block_domains']=array(
        '#type'=>'radios',
        '#maxlength' => 1,
        '#options' => array(
            0 => 'I want to allow only some of the domains to login to Drupal site',
            1 => 'I want to block some of the domains from login to Drupal site'
        ),
        '#disabled' => TRUE,
    );

    $form['miniorange_saml_set_of_radiobuttons']['miniorange_saml_domains'] = array(
			'#type' => 'textarea',
			'#title' => t(''),
			'#placeholder' => 'Enter semicolon(;) separated domains (Eg. domain1.com; domain2.com)',
			'#disabled' => true,
			'#default_value' => is_null( $config->get('miniorange_saml_domains') ) ? '' : $config->get('miniorange_saml_domains'),
			'#suffix' => '<br>',
		);

    $form['miniorange_saml_attribute_based_restriction'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Allow SSO login  based on attributes'),
      '#disabled'      => 'disabled',
    );
    $form['miniorange_saml_attribute_fieldset'] = array(
      '#type'   => 'fieldset',
      '#title' => $this->t('Conditional logic'),
    );

    $form['miniorange_saml_attribute_fieldset']['note'] =array(
    '#markup'     => '<div class="mo_saml_highlight_background_note_1">' .
      $this->t(
        ' This feature allows you to restrict SSO of the users based on IdP attributes. ' .
        'This is done by specifying logic between particular user attribute and their values.'.
        ' (SSO for all the users who does not fit in the below rules will be blocked)'
      ). '<br><br>' .
      $this->t(
        '<b>Attribute Name:</b> Name of IDP attribute on which you have to do SSO.<br>
       <b>Attribute Value:</b> Value of IDP attribute during SSO for which you have to allow SSO.<br>
        <b>Operator:</b> Logical operator to decide relation between attribute name and attribute value.'
      ).
      '</div>',
    );

    $form['miniorange_saml_attribute_fieldset']['miniorange_saml_domain_table'] = [
      '#type' => 'table',
      '#header' => [
        ['data' => $this->t('Attribute Name	'), 'width' => '35%' ],
        ['data' => $this->t('Operator'), 'width' => '20%'],
        ['data' => $this->t('Attribute Value	'), 'width' => '35%'],
        ['data' => $this->t('Operation'), 'width' => '10%'],
        ]
    ];

    $form['miniorange_saml_attribute_fieldset']['miniorange_saml_domain_table'][0]['attribute_name'] = [
      '#type' => 'textfield',
      '#disabled' => true,
      '#attributes' => [
        'placeholder' => $this->t('Enter IDP Attribute Name'),
      ],
    ];

    $form['miniorange_saml_attribute_fieldset']['miniorange_saml_domain_table'][0]['operator'] = [
      '#type' => 'select',
      '#options'=>[
        'value'=>'Start with',
        'value1'=>'Contains',
        'value2'=>'Equal to',
        'value3'=>'Ends with',
      ],
      '#attributes' => [
      'placeholder' => $this->t('select'),
      ],
    ];

    $form['miniorange_saml_attribute_fieldset']['miniorange_saml_domain_table'][0]['attribute_value'] = [
      '#type' => 'textfield',
      '#disabled' => true,
      '#attributes' => [
        'placeholder' => $this->t('Enter IdP Attribute Value'),
        ],
    ];

    $form['miniorange_saml_attribute_fieldset']['miniorange_saml_domain_table'][0]['delete_custom_attr'] = [
      '#markup' => '<span class="button">Remove</span>',
    ];

  $form['miniorange_saml_attribute_fieldset']['custom_mapping_add_button'] = [
    '#prefix' => '<div class="container-inline">',
    '#type' => 'submit',
    '#value' => 'Add',
    '#disabled' => TRUE,
  ];

  $form['miniorange_saml_attribute_fieldset']['custom_mapping_total_items'] = [
    '#type' => 'number',
    '#default_value' => 1,
    '#min' => 1,
    '#max' => 50,
    '#suffix' => '&nbsp;&nbsp;more rows</div>',
  ];

  $form['miniorange_saml_restrict_redirect_outside_domain'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Restrict redirect after SSO to outside domain'),
    '#disabled'      => true,
  );
  $form['miniorange_saml_whitelist_fieldset'] = array(
    '#type'   => 'fieldset',
    );

  $form['miniorange_saml_whitelist_fieldset']['miniorange_saml_whitelist_domains'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Whitelist Domains'),
    '#placeholder'   => 'Enter semicolon(;) separated domains (Eg. domain1.com; domain2.com)',
    '#disabled'      => 'disabled',
    '#default_value' => is_null( $config->get('miniorange_saml_whitelist_domains') ) ? '' : $config->get('miniorange_saml_whitelist_domains'),
    '#suffix'        => '<br>',
    '#description'   => 'Enter semicolon(;) separated domains (Eg. domain1.com; domain2.com)<br><b>Note:-</b> The module will redirect to the relay state iff the domain of the relay state url is whitelisted.',
  );

    $form['miniorange_saml_gateway_config_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Configuration'),
      '#disabled' => TRUE,
    );

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
  }
 }
